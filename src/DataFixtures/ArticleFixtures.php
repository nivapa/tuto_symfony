<?php

namespace App\DataFixtures;

use App\Entity\Article as Article;
use App\Entity\ArticleCategory;
use App\Entity\Comment;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ArticleFixtures extends Fixture
{
    private $encoder;
    function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        /* 
         * User
         */
        $user = new User();
        $user->setUsername('Benoit MOTTIN');
        $user->setEmail('benoit.mottin@normandie.cci.fr');
        $user->setPassword($this->encoder->encodePassword($user, 'admin123'));
        $user->setRoles(['ROLE_USER']);
        $manager->persist($user);

        $admin = new User();
        $admin->setUsername('admin');
        $admin->setEmail('admin@monsite.com');
        $admin->setPassword($this->encoder->encodePassword($admin, 'admin123'));
        $admin->setRoles(['ROLE_USER', 'ROLE_ADMIN']);
        $manager->persist($admin);

        // tableau d'user pour la ramndomisation des article et des commentaires
        $userArray = [$user, $admin];

        /*
         * category
         */
        $categories = array('politique', 'technologie', 'culture');
        $categArray = array();

        for ($i=0; $i < count($categories); $i++) { 
            
            $c = new ArticleCategory();
            $c->setName($categories[$i]);
            $categArray[] = $c;
            $manager->persist($c);            
        } //*/


        /*
         * ARTICLE
         */
        
        //on va créer 10 articles
        for ($i = 1; $i < 10; $i++) {

            $j = rand(0,count($categArray)-1);
            // random user
            $u = rand(0,count($userArray)-1);

            //j'instancie un article à partir de la class Article
            $article = new Article();
            //Avec les setter je vais donner des valeurs aux propriétés
            //je peux les enchaîner car le setter renvoie un objet Article
            $article->setTitle("Titre de l'article numéro " . $i)
                ->setContent("<em>Contenu de l'article numéro " . $i . "</em>")
                ->setImage("http://placehold.it/350x150")
                ->setCategory($categArray[$j])
                ->setAuthor($userArray[$u])
                ->setCreatedAt(new \DateTime())
                ->setUpdatedAt(new \DateTime());

            /*
             * Je vais creer des commentaires
             * que je vais lier ensuite à mes articles
             */
                for ($k=0; $k < rand(2,5); $k++) { 
                
                    $comment = new Comment();
                    $comment->setContent("C'est un commentaire, un peu bidon, mais qui va refleter un peu comme un vrai com mais il aura pour id:" . $k);
                    $comment->setArticle($article);
                    $comment->setCreatedAt(new \DateTime());
                    $comment->setAuthor($userArray[$u]->getUsername());
                    $manager->persist($comment);
                }

            //Pour l'instant je n'ai pas écrit dans la table , à chaque article créé
            //ici on utilise \DateTime pour indiquer que la classe DateTime se trouve dans le namespace
            //global de PHP
            //je vais demander à mon manager de stocker les données de cet article
            $manager->persist($article);
        }

        

        $manager->flush();
    }
}
