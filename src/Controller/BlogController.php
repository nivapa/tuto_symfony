<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Comment;
use App\Form\ArticleType;
use App\Form\CommentType;
use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class BlogController extends AbstractController
{
    protected $manager;

    public function __construct(EntityManagerInterface $entityManager)
    {
         $this->manager = $entityManager;
    }

    /**
     * 
     * @Route("/", name="home")
     */
    public function home()
    {
        return $this->render("blog/home.html.twig");
    }

    /**
     * @Route("/blog", name="blog")
     */
    public function index()
    {
        $repo = $this->getDoctrine()->getRepository(Article::class);
        $articles = $repo->findAll();
        return $this->render('blog/index.html.twig', [
            'articles' => $articles,
        ]);
    }

        /**
     * @Route("/blog/new", name="blog_create")
     * @Route("/blog/{id}/edit", name="blog_edit")
     */
    public function create(Article $article=null,Request $request){

        if(!$article){
            $article=new Article();
        }

        $edit = ($article->getId()!==null);
        $form=$this->createForm(ArticleType::class,$article);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            
            $article->setupdatedAt(new \DateTime());
            if(!$edit){
                $article->setCreatedAt(new \DateTime());
            }
            $this->manager->persist($article);
            $this->manager->flush();

            return $this->redirectToRoute('blog_show',[
                'id'=>$article->getId()
            ]);

        }

        return $this->render("blog/create.html.twig",[
            'formArticle'=>$form->createView(),
            'editMode'=> $edit
        ]);

}


    /**
     * @Route("/blog/{id}", name="blog_show")
     */
    public function show(ArticleRepository $repo, $id, Request $request)
    {
        // recueration de l'article qui corresqpond à l'id dans l'url
        $article = $repo->find($id);
        // creation d'un nouveau commentaire vide
        $comment = new Comment();
        // un creer une nouvelle de formulaire baser sur le type Comment
        $form=$this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $comment->setArticle($article);
            $comment->setCreatedAt(new \DateTime());

            $this->manager->persist($comment);
            $this->manager->flush();
        }
        return $this->render("blog/show.html.twig", [
            'article' => $article,
            // $form->createView, genere le html
            'formComment' => $form->createView()
        ]);
    }
}
